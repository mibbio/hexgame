package de.mibbiodev.hexgame.engine.event.control;

/**
 * @author mibbio
 */
public enum MouseButtonStatus {
    RELEASED,
    PRESSED
}
