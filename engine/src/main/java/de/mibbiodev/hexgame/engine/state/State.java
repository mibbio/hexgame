package de.mibbiodev.hexgame.engine.state;

import de.mibbiodev.hexgame.engine.EngineSteps;
import de.mibbiodev.hexgame.engine.control.ControlHandler;
import de.mibbiodev.hexgame.engine.screen.Screen;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;

public interface State extends EngineSteps {

    boolean activate(ShaderLibrary shaderLibrary, ControlHandler controlHandler) throws Exception;

    boolean deactivate(ShaderLibrary shaderLibrary, ControlHandler controlHandler);

    boolean pause();

    boolean resume();

    void addScreen(Screen screen);
}
