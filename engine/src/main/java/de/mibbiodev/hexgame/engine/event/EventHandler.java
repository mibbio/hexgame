package de.mibbiodev.hexgame.engine.event;

import java.lang.annotation.*;

/**
 * @author mibbio
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {

    public EventPriority priority() default EventPriority.NORMAL;
    public boolean receiveCanceled() default false;
}
