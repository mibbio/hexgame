package de.mibbiodev.hexgame.engine.state;

import de.mibbiodev.hexgame.engine.Engine;
import de.mibbiodev.hexgame.engine.World;
import de.mibbiodev.hexgame.engine.assets.ResourceLocation;
import de.mibbiodev.hexgame.engine.control.ControlHandler;
import de.mibbiodev.hexgame.engine.render.WorldRenderer;
import de.mibbiodev.hexgame.engine.screen.Screen;
import de.mibbiodev.hexgame.engine.screen.WorldScreen;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;
import de.mibbiodev.hexgame.engine.utils.Transform;
import org.lwjgl.util.vector.Vector3f;

import java.util.ArrayList;
import java.util.List;

public final class GameState implements State {

    private boolean paused = false;
    private World world;
    private List<Screen> screenList;

    private ResourceLocation worldFile;

    public GameState(ResourceLocation worldFile) {
        this.screenList = new ArrayList<>(2);
        this.worldFile = worldFile;
    }

    @Override
    public boolean activate(ShaderLibrary shaderLibrary, ControlHandler controlHandler) throws Exception {
        WorldScreen ws = new WorldScreen(new WorldRenderer(Engine.SCREEN_WIDTH, Engine.SCREEN_WIDTH, shaderLibrary));

        // FIXME Kamera-Rotation wird scheinbar nicht verarbeitet
        Transform cameraTransform = new Transform(new Vector3f(0.0f, -10.0f, 0.0f), new Vector3f(45.0f, 45.0f, 0.0f));
        ((WorldRenderer)ws.getRenderer()).initCamera(cameraTransform);
        controlHandler.addListener(ws.getRenderer().getCamera());

        this.screenList.add(ws);

        world = new World(worldFile, screenList.get(0));

        return true;
    }

    @Override
    public boolean deactivate(ShaderLibrary shaderLibrary, ControlHandler controlHandler) {
        controlHandler.removeListener(screenList.get(0).getRenderer().getCamera());
        for (Screen screen : screenList) {
            screen.cleanUp();
        }
        return true;
    }

    @Override
    public boolean pause() {
        paused = true;
        return true;
    }

    @Override
    public boolean resume() {
        paused = false;
        return true;
    }

    @Override
    public void addScreen(Screen screen) {
        if (screenList.isEmpty()) {
            screenList.add(1, screen);
        } else {
            screenList.add(screen);
        }
    }

    @Override
    public void update(int delta) {
        if (!paused) {
            for (Screen screen : screenList) {
                screen.update(delta);
            }
        }
    }

    @Override
    public void render() {
        for (Screen screen : screenList) {
            screen.render();
        }
    }

    @Override
    public void cleanUp() {
        for (Screen screen : screenList) {
            screen.cleanUp();
        }
    }
}
