package de.mibbiodev.hexgame.engine.camera;

import de.mibbiodev.hexgame.engine.control.ControlListener;
import org.lwjgl.util.vector.Vector3f;

public interface CameraInterface extends ControlListener {
    int getId();

    void setView();

    void setRotation(Vector3f rotation);

    void setPosition(Vector3f position);

    void moveAlongX(float distance);

    void moveAlongZ(float distance);

    void moveAlongY(float distance);

    void update(int delta);
}
