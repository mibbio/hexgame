package de.mibbiodev.hexgame.engine.exception;

/**
 * @author mibbio
 */
public class HexException extends Exception {

    public HexException(String message) {
        super(message);
    }
}
