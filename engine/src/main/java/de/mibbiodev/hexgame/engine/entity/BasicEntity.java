package de.mibbiodev.hexgame.engine.entity;

import de.mibbiodev.hexgame.engine.Engine;
import de.mibbiodev.hexgame.engine.assets.GameModel;
import de.mibbiodev.hexgame.engine.render.VBOData;
import de.mibbiodev.hexgame.engine.utils.Transform;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

/**
 * @author mibbio
 */
public abstract class BasicEntity implements Entity {

    // TODO shader festlegen
    public static int DEFAULT_ENTITY_SHADER = 0;

    protected Transform transform;
    protected int modelId;

    protected BasicEntity(int modelId) {
        this.modelId = modelId;
        this.transform = new Transform();
    }

    @Override
    public void render() {
        VBOData vboData = getModel().getVBOData();
        GL11.glPushMatrix();
        GL11.glTranslatef(transform.getPosition().x, transform.getPosition().y, transform.getPosition().z);
        GL11.glRotatef(transform.getRotation().x, 1, 0, 0);
        GL11.glRotatef(transform.getRotation().y, 0, 1, 0);
        GL11.glRotatef(transform.getRotation().z, 0, 0, 1);
        GL11.glScalef(transform.getScale().x, transform.getScale().y, transform.getScale().z);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboData.getBufferId());
        GL11.glVertexPointer(3, GL11.GL_FLOAT, vboData.getVertexSize() << 2, 0L);
        GL11.glNormalPointer(GL11.GL_FLOAT, vboData.getVertexSize() << 2, 3L << 2);
        GL11.glColorPointer(3, GL11.GL_FLOAT, vboData.getVertexSize() << 2, 6L << 2);
        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vboData.getElementCount());
        GL11.glPopMatrix();
    }

    @Override
    public final Transform getTransform() {
        return transform;
    }

    @Override
    public GameModel getModel() {
        return (GameModel) Engine.getCoreProxy().getAssetManager().getAssetByID(modelId);
    }
}
