package de.mibbiodev.hexgame.engine.assets;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.Color;

public class ModelVertex {

    // vertex data
    private Vector3f xyz = new Vector3f();
    private Vector3f norm = new Vector3f();
    private Color color = new Color(Color.white);

    // amount of elements of a vertex
    public static final int elementCount = 12;
    // amount of bytes for one element
    public static final int elementBytes = 4;
    // size of a vertex in bytes
    public static final int sizeInBytes = elementBytes * elementCount;

    // Setters
    public ModelVertex setXYZ(float x, float y, float z) {
        this.xyz = new Vector3f(x, y, z);
        return this;
    }

    public ModelVertex setXYZ(Vector3f vector) {
        this.setXYZ(vector.x, vector.y, vector.z);
        return this;
    }

    public ModelVertex setColor(float r, float g, float b, float a) {
        this.color = new Color(r, g, b, a);
        return this;
    }

    public ModelVertex setColor(float r, float g, float b) {
        this.setColor(r, g, b, 1f);
        return this;
    }

    public ModelVertex setColor(Color color) {
        this.color = color;
        return this;
    }

    public ModelVertex setNormal(float nx, float ny, float nz) {
        this.norm = new Vector3f(nx, ny, nz);
        return this;
    }

    public ModelVertex setNormal(Vector3f normal) {
        this.setNormal(normal.x, normal.y, normal.z);
        return this;
    }

    // Getters
    public Vector3f getXYZ() {
        return new Vector3f(this.xyz);
    }

    public Color getColor() {
        return this.color;
    }

    public Vector3f getNormal() {
        return new Vector3f(this.norm);
    }
}
