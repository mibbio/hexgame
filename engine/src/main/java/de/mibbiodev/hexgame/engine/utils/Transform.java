package de.mibbiodev.hexgame.engine.utils;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public final class Transform {

    private Vector3f position = new Vector3f(0, 0, 0);
    private Vector3f rotation = new Vector3f(0, 0, 0);
    private Vector3f scale = new Vector3f(1, 1, 1);

    public Transform() {}

    public Transform(Vector3f position, Vector3f rotation) {
        this.position = position;
        this.rotation = rotation;
    }

    public Transform(Vector3f position, Vector3f rotation, Vector3f scale) {
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }

    public final Vector3f getPosition() {
        return position;
    }

    public final void setPosition(Vector3f position) {
        this.position = position;
    }

    public final Vector3f getRotation() {
        return rotation;
    }

    public final void setRotation(Vector3f rotation) {
        this.rotation = rotation;
    }

    public final void rotate(float x, float y, float z) {
        float xn = rotation.getX() + x;
        float yn = rotation.getY() + y;
        float zn = rotation.getZ() + z;

        xn = xn > 360 ? xn - 360 : xn;
        yn = yn > 360 ? yn - 360 : yn;
        zn = zn > 360 ? zn - 360 : zn;

        rotation.set(xn, yn, zn);
    }

    public final void translate(float x, float y, float z) {
        position.translate(x, y, z);
    }

    public final void scale(float x, float y, float z) {
        if (x == 0 || y == 0 || z == 0) throw new IllegalArgumentException("Scale by zero is not allowed.");
        scale.x *= x;
        scale.y *= y;
        scale.z *= z;
    }

    public final void scale(float value) {
        if (value == 0) throw new IllegalArgumentException("Scale by zero is not allowed.");
        scale.x *= value;
        scale.y *= value;
        scale.z *= value;
    }

    public final Vector3f getScale() {
        return scale;
    }

    public final void setScale(Vector3f scale) {
        this.scale = scale;
    }

    public final Matrix4f asMatrix4f() {
        return new Matrix4f();
    }
}
