package de.mibbiodev.hexgame.engine.render;

import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;
import org.newdawn.slick.Color;

/**
 * @author mibbio
 */
public interface Renderable {

    public enum RenderStage {
        PREPARE_STAGE,
        PICKING_STAGE,
        MAIN_STAGE,
        POST_STAGE
    }

    void update(int delta);

    void pick(Color color);

    void drawMesh(RenderStage renderStage);

    void activateShader(ShaderLibrary shaderLibrary, RenderStage renderStage);

    void setupShader(ShaderLibrary shaderLibrary);

    void cleanUp(ShaderLibrary shaderLibrary);
}
