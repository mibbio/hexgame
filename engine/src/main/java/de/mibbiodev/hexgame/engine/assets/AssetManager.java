package de.mibbiodev.hexgame.engine.assets;

import java.io.File;
import java.util.*;

public final class AssetManager {

    private final AssetLoader loader;
    private int lastId = 0;

	/*
	 * TODO Verwalten der VBOs im AssetManager
	 * - beim Laden eines Models pr�fen ob schon ein VBO existiert, in das das Model reinpasst
	 * - wenn nicht, neues VBO erstellen
	 * - Verweis auf das richtige VBO und die Position der Daten darin mit dem Model verbinden
	 */

    private final Map<String, Icon> asset_icons;
    private final Map<String, GameModel> asset_models;

    public AssetManager() {
        this.loader = new AssetLoader();
        this.asset_icons = new TreeMap<>();
        this.asset_models = new TreeMap<>();
    }

    public final int getAsset(AssetType type, String name, ResourceLocation resourceLocation) {
        int id = 0;
        switch (type) {
            case ICON:
                if (this.asset_icons.containsKey(resourceLocation.toString())) {
                    id = this.asset_icons.get(resourceLocation.toString()).getId();
                } else if (this.lastId < Integer.MAX_VALUE) {
                    this.lastId++;
                    Icon icon = loader.loadPNG(lastId, name, resourceLocation);
                    this.asset_icons.put(resourceLocation.toString(), icon);
                    id = this.lastId;
                }
                break;
            case MODEL:
                if (this.asset_models.containsKey(resourceLocation.toString())) {
                    id = this.asset_models.get(resourceLocation.toString()).getId();
                } else if (this.lastId < Integer.MAX_VALUE) {
                    this.lastId++;
                    GameModel model = loader.loadOBJ(this.lastId, name, resourceLocation);
                    this.asset_models.put(resourceLocation.toString(), model);
                    id = this.lastId;
                }
                break;
            default:
                break;
        }
        return id;
    }

    public final GameAsset getAssetByID(int id) {
        Set<GameAsset> allAssets = new TreeSet<>();
        allAssets.addAll(this.asset_icons.values());
        allAssets.addAll(this.asset_models.values());

        for (GameAsset asset : allAssets) {
            if (asset.getId() == id) return asset;
        }
        return null;
    }

    public final GameAsset getAssetByName(String name) {
        Set<GameAsset> allAssets = new TreeSet<>();
        allAssets.addAll(this.asset_icons.values());
        allAssets.addAll(this.asset_models.values());
        for (GameAsset asset : allAssets) {
            if (asset.getName().equals(name)) return asset;
        }
        return null;
    }
}
