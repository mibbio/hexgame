package de.mibbiodev.hexgame.engine.render;

import de.mibbiodev.hexgame.engine.EngineSteps;
import de.mibbiodev.hexgame.engine.camera.CameraInterface;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;
import org.newdawn.slick.Color;

/**
 * @author mibbio
 */
public interface Renderer extends EngineSteps {

    ShaderLibrary getShaderLibrary();

    void startRendering();

    void enablePick(int x, int y);

    Color pickingPass(int x, int y);

    void disablePick();

    void endRendering();

    void postProcesss();

    CameraInterface getCamera();
}
