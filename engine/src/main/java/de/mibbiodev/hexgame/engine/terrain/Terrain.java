package de.mibbiodev.hexgame.engine.terrain;

//import World;

import de.mibbiodev.hexgame.engine.Engine;
import de.mibbiodev.hexgame.engine.assets.GameModel;
import de.mibbiodev.hexgame.engine.assets.ResourceLocation;
import de.mibbiodev.hexgame.engine.render.Renderable;
import de.mibbiodev.hexgame.engine.shader.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.*;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.Color;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public final class Terrain implements Renderable {
    private TerrainTile[] tiles;

    private final int width;
    private final int height;

    // vbo data
    private int vboVertices = 0;
    private int vboIndices = 0;
    private IntBuffer indexData;

    private final int vertexSize;

    // shader
    private int selectionShader;
    private int renderShader;

    // testdata
    int selection = 0;
    ShaderParam<Float> selectionParam = new ShaderParam<>("Selection", (float) this.selection);

    public Terrain(int width, int height, TerrainTile[][] tileData) throws Exception {
        if (tileData.length < width || tileData[0].length < height) {
            throw new Exception("Number of tiles in tileData doesn't fit with terrainsize");
        }

        this.width = width;
        this.height = height;

        int vertexAmount = width * height * 12;
        int indexAmount = width * height * 48;
        // 3 vertex coords // 3 normal coords // 3 1st-color values //  3 2nd-color value
        vertexSize = GameModel.VERTEX_SIZE + 3;

        //final float sideLength = 1.0f;
        final float sideLength = TerrainTile.SIDE_LENGTH;
        final float tileGap = 0.0f;

        FloatBuffer vertexData = BufferUtils.createFloatBuffer(vertexAmount * vertexSize);
        indexData = BufferUtils.createIntBuffer(indexAmount);

        tiles = new TerrainTile[width * height];
        float h = (float) (Math.sin(Math.toRadians(30)) * sideLength);
        float r = (float) (Math.cos(Math.toRadians(30)) * sideLength);
        for (int x = 0; x < this.width; x++) {
            for (int z = 0; z < this.height; z++) {
                tiles[z * this.width + x] = tileData[x][z];

                if (x % 2 != 0) {
                    tiles[z * this.width + x].setPosition(
                            x * (h + sideLength + tileGap),
                            0.0f,
                            -z * (2 * r + tileGap) + r + tileGap / 2.0f);
                } else {
                    tiles[z * this.width + x].setPosition(
                            x * (h + sideLength + tileGap),
                            0.0f,
                            -z * (2 * r + tileGap));
                }

                Vector3f[] vertices = tiles[z * this.width + x].getVertices();
                for (Vector3f v : vertices) {
                    // Vertex
                    vertexData.put(v.x);
                    vertexData.put(v.y);
                    vertexData.put(v.z);

                    // Normal
                    vertexData.put(0.0f);
                    vertexData.put(1.0f);
                    vertexData.put(0.0f);

                    // Picking Color
                    vertexData.put(x / 100.0f); // red
                    vertexData.put(0.1f);       // green
                    vertexData.put(z / 100.0f); // blue

                    // Visible Color
                    Color color = tiles[z * this.width + x].getColor();
                    vertexData.put(color.r);    // red
                    vertexData.put(color.g);    // green
                    vertexData.put(color.b);    // blue
                    // TODO Texture coords
                }
                indexData.put(tiles[z * this.width + x].getIndices((short) (z * this.width + x)));
            }
        }

        vertexData.flip();
        indexData.flip();

        vboVertices = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboVertices);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexData, GL15.GL_STATIC_DRAW);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        vboIndices = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboIndices);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indexData, GL15.GL_STATIC_DRAW);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    @Override
    public void update(int delta) {
    }

    @Override
    public void pick(Color color) {
        if (color.g > 0.0f) {
            selection = Math.round(color.b * 100.0f) * width + Math.round(color.r * 100.0f);
        } else {
            selection = -1;
        }
    }

    @Override
    public void drawMesh(RenderStage renderStage) {
        GL11.glPushMatrix();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboVertices);
        GL11.glVertexPointer(3, GL11.GL_FLOAT, vertexSize << 2, 0L);
        GL11.glNormalPointer(GL11.GL_FLOAT, vertexSize << 2, 3L << 2);
        GL11.glColorPointer(3, GL11.GL_FLOAT, vertexSize << 2, 6L << 2);
        GL14.glSecondaryColorPointer(3, GL11.GL_FLOAT, vertexSize << 2, 9L << 2);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboIndices);
        GL11.glDrawElements(GL11.GL_TRIANGLES, indexData.capacity(), GL11.GL_UNSIGNED_INT, 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        GL11.glPopMatrix();
    }

    @Override
    public void activateShader(ShaderLibrary shaderLibrary, RenderStage renderStage) {
        switch (renderStage) {
            case PICKING_STAGE:
                shaderLibrary.activateShaderProgram(selectionShader);
                break;
            case MAIN_STAGE:
                selectionParam.setValue((float) selection);
                shaderLibrary.setShaderParam(renderShader, selectionParam);
                shaderLibrary.activateShaderProgram(renderShader);
                break;
            default:
        }
    }

    @Override
    public void setupShader(ShaderLibrary shaderLibrary) {
        // rendershader
        ShaderProgram shader = new ShaderProgram();

        ResourceLocation terrainFinalVert = new ResourceLocation("/glsl/terrain_final.vs.glsl", Engine.getCoreProxy().getRootClass());
        ResourceLocation terrainFinalFrag = new ResourceLocation("/glsl/terrain_final.fs.glsl", Engine.getCoreProxy().getRootClass());
        ResourceLocation terrainSelectVert = new ResourceLocation("/glsl/terrain_selection.vs.glsl", Engine.getCoreProxy().getRootClass());
        ResourceLocation terrainSelectFrag = new ResourceLocation("/glsl/terrain_selection.fs.glsl", Engine.getCoreProxy().getRootClass());

        shader.attachShader(terrainFinalVert, GL20.GL_VERTEX_SHADER);
        shader.attachShader(terrainFinalFrag, GL20.GL_FRAGMENT_SHADER);
        shader.addParameter(new ShaderParam<>("Selection", 0.0f));
        shader.addParameter(new ShaderParam<>("LightPosition", new Vector3f(0.0f, 10.0f, 0.0f)));
        shader.addParameter(new ShaderParam<>("SurfaceColor", new Vector3f(0.75f, 0.75f, 0.75f)));
        shader.addParameter(new ShaderParam<>("WarmColor", new Vector3f(0.6f, 0.6f, 0.6f)));
        shader.addParameter(new ShaderParam<>("CoolColor", new Vector3f(0.3f, 0.3f, 0.3f)));
        shader.addParameter(new ShaderParam<>("DiffuseWarm", 0.6f));
        shader.addParameter(new ShaderParam<>("DiffuseCool", 0.2f));
        renderShader = shaderLibrary.addShaderProgram(shader);

        // selectionshader
        shader = new ShaderProgram();
        shader.attachShader(terrainSelectVert, GL20.GL_VERTEX_SHADER);
        shader.attachShader(terrainSelectFrag, GL20.GL_FRAGMENT_SHADER);
        selectionShader = shaderLibrary.addShaderProgram(shader);
    }

    @Override
    public void cleanUp(ShaderLibrary shaderLibrary) {
        // dispose used shader
        shaderLibrary.removeShaderProgram(selectionShader);
        shaderLibrary.removeShaderProgram(renderShader);

        // dispose used VBOs
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        if (vboVertices > 0) {
            GL15.glDeleteBuffers(vboVertices);
        }
        if (vboIndices > 0) {
            GL15.glDeleteBuffers(vboIndices);
        }
    }

    public Vector3f getTileCenter(int tileNumber) {
        if (this.tiles.length >= tileNumber && tileNumber >= 0) {
            return this.tiles[tileNumber].getPosition();
        }
        return new Vector3f(0, 0, 0);
    }

}
