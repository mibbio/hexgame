package de.mibbiodev.hexgame.engine.state;

import de.mibbiodev.hexgame.engine.control.ControlHandler;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;

/**
 * @author mibbio
 */
public abstract class MenuState implements State {

    private boolean paused = true;

    @Override
    public boolean activate(ShaderLibrary shaderLibrary, ControlHandler controlHandler) throws Exception {
        paused = false;
        return false;  // TODO Auto-generated method stub
    }

    @Override
    public boolean deactivate(ShaderLibrary shaderLibrary, ControlHandler controlHandler) {
        return false;  // TODO Auto-generated method stub
    }

    @Override
    public boolean pause() {
        // TODO Auto-generated method stub
        paused = true;
        return false;
    }

    @Override
    public boolean resume() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void update(int delta) {
        // TODO Auto-generated method stub
    }
}
