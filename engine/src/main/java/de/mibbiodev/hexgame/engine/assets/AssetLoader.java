package de.mibbiodev.hexgame.engine.assets;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public final class AssetLoader {

    public GameModel loadOBJ(int modelId, String modelName, ResourceLocation resourceLocation) {
        /* - v	= vertex
		 * - vn = normal
		 * - vt	= texture coordinate
		 * - f	= faces (primitives) as vertex indices
		 * - vertice lines:	"v 	x, 	y, 	z [, w]"
		 * - normal lines:	"vn	x, 	y,	z [, w]"
		 * - face lines:	"f	v1/vt1/vn1   v2/vt2/vn2   v3/vt3/vn3"
		 */
		
		/*
		 * Export aus Cinema4D
		 * -------------------
		 * 1. Object modelieren
		 * 2. Alles triangulieren
		 * 3. Alle Flaechen markieren
		 * 4. Abloesen
		 * 5. Export als fbx
		 * 6. FBX Converter -> OBJ (ohne nochmaliges Triangulieren)
		 */

        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(resourceLocation.asInputStream()));
        } catch (FileNotFoundException e1) {
            return null;
        }

        List<Vector3f> vertexValues = new ArrayList<>();
        List<Vector3f> normalValues = new ArrayList<>();
        List<ModelFace> faceIndices = new ArrayList<>();

        try {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("v ")) {
                    // read vertices
                    String[] values = line.split(" ");
                    float vx = Float.parseFloat(values[1]) / 20f;
                    float vy = Float.parseFloat(values[2]) / 20f;
                    float vz = Float.parseFloat(values[3]) / 20f;
                    vertexValues.add(new Vector3f(vx, vy, vz));

                } else if (line.startsWith("vn ")) {
                    // read normals
                    String[] values = line.split(" ");
                    float nx = Float.parseFloat(values[1]);
                    float ny = Float.parseFloat(values[2]);
                    float nz = Float.parseFloat(values[3]);
                    normalValues.add(new Vector3f(nx, ny, nz));

                } else if (line.startsWith("f ")) {
                    // read faces
                    int[] v = new int[3];
                    int[] n = new int[3];
                    String[] faceDef = line.split(" ");
                    for (int i = 1; i < faceDef.length; i++) {
                        v[i - 1] = Integer.valueOf(faceDef[i].split("/")[0]) - 1;
                        n[i - 1] = Integer.valueOf(faceDef[i].split("/")[2]) - 1;
                    }
                    faceIndices.add(new ModelFace(v[0], v[1], v[2], n[0], n[1], n[2]));
                }
            }
        } catch (NumberFormatException | IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new GameModel(modelId, modelName, vertexValues, normalValues, faceIndices);
    }

    public Icon loadPNG(int iconId, String iconName, ResourceLocation resourceLocation) {
        // TODO überarbeiten: anstatt File die ResourceLocation nutzen
        Icon icon = null;
        try {
            icon = new Icon(iconId, iconName, TextureLoader.getTexture("PNG", resourceLocation.asInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return icon;
    }
}
