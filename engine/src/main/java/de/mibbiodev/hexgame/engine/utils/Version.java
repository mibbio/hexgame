package de.mibbiodev.hexgame.engine.utils;

import java.text.DecimalFormat;
import java.util.Arrays;

/**
 * @author mibbio
 */
public class Version implements Comparable {
    private final Integer[] versionNumber;
    private final Integer buildNumber;
    private final String snapshotName;

    private char numberSeparator = '.';
    private char buildSeparator = '-';
    private int maxDigitsPerSequence = 2;

    public Version(Integer buildNumber, Integer... versionParts) {
        this.versionNumber = versionParts;
        this.buildNumber = buildNumber;
        this.snapshotName = null;
    }

    public Version(String snapshotName, Integer... versionParts) {
        this.versionNumber = versionParts;
        this.buildNumber = null;
        this.snapshotName = snapshotName;
    }

    public final Version setMaxDigitsPerSequence(int digits) {
        maxDigitsPerSequence = digits;
        return this;
    }

    public final char getBuildSeparator() {
        return buildSeparator;
    }

    public final Version setBuildSeparator(char buildSeparator) {
        this.buildSeparator = buildSeparator;
        return this;
    }

    public final char getNumberSeparator() {
        return numberSeparator;
    }

    public final Version setNumberSeparator(char numberSeparator) {
        this.numberSeparator = numberSeparator;
        return this;
    }

    public final Integer[] getVersionNumber() {
        return versionNumber.clone();
    }

    public final Integer getBuildNumber() {
        return buildNumber;
    }

    public String toFullString() {
        StringBuilder out = new StringBuilder();

        for (int i = 0; i < versionNumber.length - 1; i++) {
            out.append(versionNumber[i]).append(getNumberSeparator());
        }
        out.append(versionNumber[versionNumber.length - 1]);

        if (buildNumber != null && !buildNumber.equals(0)) {
            out.append(getBuildSeparator()).append(buildNumber);
        }

        if (snapshotName != null) {
            out.append(getBuildSeparator()).append(snapshotName);
        }

        return out.toString();
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();

        for (int part : versionNumber) {
            out.append(zeroFilledString(part, maxDigitsPerSequence));
        }

        if (buildNumber != null && !buildNumber.equals(0)) {
            out.append(".").append(buildNumber);
        }
        return out.toString();
    }

    @Override
    public int compareTo(Object obj) {
        if (obj == null) throw new NullPointerException();
        if (!(obj instanceof Version)) throw new IllegalArgumentException("Argument is no version.");

        // checking if versions have different count of parts
        int partsToCheck = 0;
        if (this.versionNumber.length > ((Version) obj).getVersionNumber().length) {
            partsToCheck = ((Version) obj).getVersionNumber().length;
        } else if (this.versionNumber.length < ((Version) obj).getVersionNumber().length) {
            partsToCheck = this.versionNumber.length;
        }

        // both versions have same amount of parts
        if (partsToCheck == 0) {
            Double thisVersion = Double.parseDouble(this.toString());
            Double thatVersion = Double.parseDouble(obj.toString());
            return thisVersion.compareTo(thatVersion);
        }

        // one version has more parts than the other
        Integer[] thisNumber = new Integer[partsToCheck];
        Integer[] thatNumber = new Integer[partsToCheck];
        for (int i = 0; i < partsToCheck; i++) {
            thisNumber[i] = this.versionNumber[i];
            thatNumber[i] = ((Version) obj).getVersionNumber()[i];
        }

        Double thisVersion = Double.parseDouble(new Version(this.getBuildNumber(), thisNumber).toString());
        Double thatVersion = Double.parseDouble(new Version(((Version) obj).getBuildNumber(), thatNumber).toString());
        int result = thisVersion.compareTo(thatVersion);

        // shared versionpart is equal -> version with more parts is greater
        if (result == 0) {
            result = this.versionNumber.length - ((Version) obj).getVersionNumber().length;
        }

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof Version && this.compareTo(obj) == 0;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    static String zeroFilledString(int number, int digits) {
        assert digits > 0 : "Invalid number of digits";
        char[] zeros = new char[digits];
        Arrays.fill(zeros, '0');
        DecimalFormat decimalFormat = new DecimalFormat(String.valueOf(zeros));
        return decimalFormat.format(number);
    }
}

