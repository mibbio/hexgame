package de.mibbiodev.hexgame.engine.state;

import de.mibbiodev.hexgame.engine.control.ControlHandler;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;

public final class StateManager {
    private State currentState = null;
    private State lastState = null;
    private ShaderLibrary shaderLibrary;
    private ControlHandler controlHandler;

    public StateManager(ShaderLibrary shaderLibrary, ControlHandler controlHandler) {
        this.shaderLibrary = shaderLibrary;
        this.controlHandler = controlHandler;
    }

    public boolean setState(State state) {
        if (state == null) return false;

        if (currentState != null) {
            currentState.deactivate(shaderLibrary, controlHandler);
        }
        lastState = currentState;
        try {
            state.activate(shaderLibrary, controlHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
        currentState = state;
        return true;
    }

    public State getCurrentState() {
        return this.currentState;
    }

    public boolean hasStates() {
        if (currentState == null && lastState == null) {
            return false;
        } else {
            return true;
        }
    }

    public void cleanUp() {
        if (lastState != null) {
            lastState.cleanUp();
            lastState = null;
        }

        if (currentState != null) {
            currentState.cleanUp();
            currentState = null;
        }
    }
}
