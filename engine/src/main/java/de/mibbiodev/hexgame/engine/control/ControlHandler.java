package de.mibbiodev.hexgame.engine.control;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.util.ArrayList;
import java.util.List;

public class ControlHandler {
    private final List<ControlListener> listeners;

    private boolean mouseBtnLeft;
    private boolean mouseBtnRight;

    public ControlHandler() {
        listeners = new ArrayList<>();
    }

    public void addListener(ControlListener controlListener) {
        if (controlListener != null) {
            this.listeners.add(controlListener);

            StringBuilder keys = new StringBuilder(controlListener.getClass().getSimpleName());
            keys.append(": ");
            for (Integer i : controlListener.getRequestedPolls()) {
                keys.append(i).append(" ");
            }
        }
    }

    public void removeListener(ControlListener controlListener) {
        if (controlListener != null) {
            listeners.remove(controlListener);
        }
    }

    public void updateInput(int delta) {
        mouseBtnLeft = Mouse.isButtonDown(0);
        mouseBtnRight = Mouse.isButtonDown(1);
        for (ControlListener l : listeners) {
            if (l.usesMouse()) {
                l.mouseButtonStatus(mouseBtnLeft, mouseBtnRight);
            }
        }

        while (Keyboard.next()) {
            int keyCode = Keyboard.getEventKey();
            if (Keyboard.getEventKeyState()) {
                System.out.println(Keyboard.getKeyName(keyCode) + " was pressed.");
            } else {
                System.out.println(Keyboard.getKeyName(keyCode) + " was released.");
            }
        }
    }
}
