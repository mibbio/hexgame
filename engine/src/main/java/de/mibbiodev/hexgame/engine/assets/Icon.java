package de.mibbiodev.hexgame.engine.assets;

import org.newdawn.slick.opengl.Texture;


public class Icon implements GameAsset {

    private final int iconID;
    private final String name;
    private Texture img;

    public Icon(int id, String name, Texture img) {
        this.iconID = id;
        this.name = name;
    }

    @Override
    public int getId() {
        return this.iconID;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public final int compareTo(GameAsset asset) {
        return this.name.compareToIgnoreCase(asset.getName());
    }

    @Override
    public final boolean equals(Object obj) {
        if (obj instanceof Icon) {
            Icon icon = (Icon) obj;
            return icon.getName().equals(this.name);
        }
        return false;
    }

    @Override
    public final int hashCode() {
        return this.name.hashCode();
    }
}
