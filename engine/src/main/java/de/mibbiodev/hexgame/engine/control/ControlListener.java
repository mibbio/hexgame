package de.mibbiodev.hexgame.engine.control;

/**
 * @author mibbio
 */
public interface ControlListener {
    /*
     * TODO Tastenverwaltung und Listenerzuordnung
     * - ID Generator schreiben, der freie IDs erzeugt
     * - Alle Spielobjekte in ner Hashmap verwalten mit ID als Key
     * - Spielobjekte erhalten einen Tag "OBJECT_TYPE" (zb. "CAMERA", "ENTITY" usw.)
     * - IDs fuer jeden ControlListener und jedes Objekt
     */

    Integer[] getRequestedPolls();

    Integer[] getRequestedEvents();

    boolean usesMouse();

    void mouseButtonStatus(boolean buttonLeft, boolean buttonRight);
}
