package de.mibbiodev.hexgame.engine.assets;

/**
 * User: mibbio
 * Date: 23.02.13
 * Time: 18:40
 */
public enum AssetType {
    ICON, MODEL, SOUND, MUSIC
}
