package de.mibbiodev.hexgame.engine.shader;

import de.mibbiodev.hexgame.engine.utils.UniqueIndexMap;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL20.glUseProgram;

public final class ShaderLibrary {
    // TODO nutzeranzahl für nen shader merken und shader nur verwerfen wenn kein nutzer mehr

    private final UniqueIndexMap<Integer, ShaderProgram> shaderlist;
    private int lastActiveShader = 0;

    public ShaderLibrary() {
        this.shaderlist = new UniqueIndexMap<>(1);
    }

    public int addShaderProgram(ShaderProgram shaderProgram) {
        int val = this.shaderlist.getKeyByValue(shaderProgram);
        if (val >= 0) {
            return val;
        }
        return this.shaderlist.put(shaderProgram);
    }

    public void setShaderParam(int shaderID, ShaderParam<?> param) {
        ShaderProgram sp = this.shaderlist.get(shaderID);
        sp.setParam(param);
    }

    public void activateShaderProgram(int shaderID) {
        if (lastActiveShader != shaderID) {
            if (shaderID == 0) {
                glUseProgram(shaderID);
                lastActiveShader = 0;
                return;
            }
            ShaderProgram sp = shaderlist.get(shaderID);
            glUseProgram(sp.getHandle());
            sp.activateParams();
            lastActiveShader = shaderID;
        }
    }

    public void removeShaderProgram(int shaderID) {
        ShaderProgram sp = this.shaderlist.remove(shaderID);
        if (sp == null) {
            Logger.getAnonymousLogger().log(Level.WARNING, "There is no shader with ID \"{0}\".", shaderID);
        } else {
            sp.cleanUpShader();
        }
    }

    public void cleanUp() {
        for (ShaderProgram sp : this.shaderlist.values()) {
            sp.cleanUpShader();
        }
        this.shaderlist.clear();
    }
}
