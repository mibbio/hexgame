package de.mibbiodev.hexgame.engine.terrain;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.Color;

public abstract class TerrainTile {

    // TODO Setting color for each tile depending on type and/or height. Use for shader.

    public static float SIDE_LENGTH = 1.0f;

    private float height;
    private Vector3f pos;
    private final int VERT = 12;

    private float speedModifier = 0.0f;

    public TerrainTile(float height) {
        this.height = height;
        this.pos = new Vector3f(0, 0, 0);
    }

    public void update(int delta) {
    }

    ;

    public Vector3f[] getVertices() {
        float h = (float) (Math.sin(Math.toRadians(30)) * SIDE_LENGTH);
        float r = (float) (Math.cos(Math.toRadians(30)) * SIDE_LENGTH);

        Vector3f[] vertices = new Vector3f[VERT];

        // Top vertices
        vertices[0] = new Vector3f(pos.x, pos.y + height, pos.z);
        vertices[1] = new Vector3f(pos.x + h, pos.y + height, pos.z + r);
        vertices[2] = new Vector3f(pos.x + h + SIDE_LENGTH, pos.y + height, pos.z + r);
        vertices[3] = new Vector3f(pos.x + 2 * h + SIDE_LENGTH, pos.y + height, pos.z);
        vertices[4] = new Vector3f(pos.x + h + SIDE_LENGTH, pos.y + height, pos.z - r);
        vertices[5] = new Vector3f(pos.x + h, pos.y + height, pos.z - r);

        vertices[6] = new Vector3f(pos.x, pos.y, pos.z);
        vertices[7] = new Vector3f(pos.x + h, pos.y, pos.z + r);
        vertices[8] = new Vector3f(pos.x + h + SIDE_LENGTH, pos.y, pos.z + r);
        vertices[9] = new Vector3f(pos.x + 2 * h + SIDE_LENGTH, pos.y, pos.z);
        vertices[10] = new Vector3f(pos.x + h + SIDE_LENGTH, pos.y, pos.z - r);
        vertices[11] = new Vector3f(pos.x + h, pos.y, pos.z - r);

        return vertices;
    }

    public Vector3f[] getNormals() {
        Vector3f[] normals = new Vector3f[VERT];
        for (int i = 0; i < 12; i++) {
            normals[i] = new Vector3f(0, 1, 0);
        }
        return normals;
    }

    public int[] getIndices(short tileNumber) {
        int offset = tileNumber * VERT;

        return new int[]{
                0 + offset, 1 + offset, 5 + offset,
                5 + offset, 1 + offset, 4 + offset,
                4 + offset, 1 + offset, 2 + offset,
                2 + offset, 3 + offset, 4 + offset,

                0 + offset, 6 + offset, 1 + offset,
                1 + offset, 6 + offset, 7 + offset,

                1 + offset, 7 + offset, 2 + offset,
                2 + offset, 7 + offset, 8 + offset,

                2 + offset, 8 + offset, 3 + offset,
                3 + offset, 8 + offset, 9 + offset,

                3 + offset, 9 + offset, 4 + offset,
                4 + offset, 9 + offset, 10 + offset,

                4 + offset, 10 + offset, 5 + offset,
                5 + offset, 10 + offset, 11 + offset,

                5 + offset, 11 + offset, 0 + offset,
                0 + offset, 11 + offset, 6 + offset
        };
    }

    public void setPosition(float x, float y, float z) {
        pos.set(x, y, z);
    }

    public Vector3f getPosition() {
        return this.pos;
    }

    public float getSpeedModifier() {
        return speedModifier;
    }

    public void setSpeedModifier(float speedModifier) {
        this.speedModifier = speedModifier;
    }

    public abstract Color getColor();


}
