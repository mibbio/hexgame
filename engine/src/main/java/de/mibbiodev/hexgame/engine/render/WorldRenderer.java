package de.mibbiodev.hexgame.engine.render;

import de.mibbiodev.hexgame.engine.camera.CameraInterface;
import de.mibbiodev.hexgame.engine.camera.CameraPerspective;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;
import de.mibbiodev.hexgame.engine.utils.Transform;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.newdawn.slick.Color;

import java.nio.FloatBuffer;

/**
 * @author mibbio
 */
public class WorldRenderer implements Renderer {

    protected CameraPerspective currentCamera;
    protected ShaderLibrary shaderLibrary;

    public WorldRenderer(int width, int height, ShaderLibrary shaderLibrary) {
        this.currentCamera = new CameraPerspective(70, (float) width / (float) height, 1.0f, 1000.0f);
        this.shaderLibrary = shaderLibrary;
    }

    public void initCamera(Transform transform) {
        currentCamera.initProjection();
        currentCamera.setRotation(transform.getRotation());
        currentCamera.setPosition(transform.getPosition());

        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDepthFunc(GL11.GL_LEQUAL);
        GL11.glDepthRange(0.0, 1.0);
        //GL11.glEnable(GL11.GL_CULL_FACE);
        //GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
    }

    @Override
    public void update(int delta) {
        currentCamera.update(delta);
    }

    @Override
    public ShaderLibrary getShaderLibrary() {
        return shaderLibrary;
    }

    @Override
    public CameraInterface getCamera() {
        return currentCamera;
    }

    @Override
    public void startRendering() {
        GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
        GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
        GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
        GL11.glEnableClientState(GL14.GL_SECONDARY_COLOR_ARRAY);
    }

    @Override
    public void enablePick(int x, int y) {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
        GL11.glScissor(x, y, 1, 1);
    }

    @Override
    public Color pickingPass(int x, int y) {
        FloatBuffer pixels = BufferUtils.createFloatBuffer(3);
        GL11.glReadPixels(x, y, 1, 1, GL11.GL_RGB, GL11.GL_FLOAT, pixels);
        pixels.rewind();
        return new Color(pixels.get(0), pixels.get(1), pixels.get(2));
    }

    @Override
    public void disablePick() {
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

    @Override
    public void render() {
        currentCamera.setView();
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

    }

    @Override
    public void endRendering() {
        GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
        GL11.glDisableClientState(GL11.GL_NORMAL_ARRAY);
        GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);
        GL11.glDisableClientState(GL14.GL_SECONDARY_COLOR_ARRAY);
    }

    @Override
    public void postProcesss() {}

    @Override
    public void cleanUp() {}
}
