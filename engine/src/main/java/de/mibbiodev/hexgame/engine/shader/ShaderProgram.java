package de.mibbiodev.hexgame.engine.shader;

import de.mibbiodev.hexgame.engine.assets.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.*;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class ShaderProgram {

    private int shaderHandle;
    private Map<Integer, ShaderParam<?>> parameters;
    private List<ResourceLocation> attachedShaderFiles;
    private List<Integer> attachedShaderIds;

    public ShaderProgram() {
        this.parameters = new HashMap<>();
        this.attachedShaderFiles = new ArrayList<>();
        this.attachedShaderIds = new ArrayList<>();
        this.shaderHandle = GL20.glCreateProgram();
    }

    public void attachShader(ResourceLocation shaderLocation, int shaderType) {
        if ((shaderType == GL20.GL_VERTEX_SHADER) || (shaderType == GL20.GL_FRAGMENT_SHADER)) {
            String source = ShaderProgram.loadShaderSource(shaderLocation);

            if (source.contains("main()")) {
                int shader = GL20.glCreateShader(shaderType);

                GL20.glShaderSource(shader, source);
                GL20.glCompileShader(shader);
                if (GL20.glGetShaderi(shader, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
                    Logger.getAnonymousLogger().log(Level.SEVERE, "Error in shader: {0}", GL20.glGetShaderInfoLog(shader, 1000));
                }

                GL20.glAttachShader(this.shaderHandle, shader);
                GL20.glLinkProgram(this.shaderHandle);
                GL20.glValidateProgram(this.shaderHandle);
                GL20.glDeleteShader(shader);
                this.attachedShaderFiles.add(shaderLocation);
                this.attachedShaderIds.add(shader);
            } else {
                Logger.getAnonymousLogger().log(Level.SEVERE, "File contains no shadersource.");
            }
        } else {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Wrong shadertype. Use 'GL_VERTEX_SHADER' or 'GL_FRAGMENT_SHADER'");
        }
    }


    public void cleanUpShader() {
        for (int sid : this.attachedShaderIds) {
            GL20.glDetachShader(this.shaderHandle, sid);
        }
        GL20.glDeleteProgram(this.shaderHandle);
    }

    public int getHandle() {
        return this.shaderHandle;
    }

    private List<ResourceLocation> getAttachedShader() {
        return this.attachedShaderFiles;
    }

    public ShaderProgram addParameter(ShaderParam<?> param) {
        int location = GL20.glGetUniformLocation(this.shaderHandle, param.getName());
        this.parameters.put(location, param);
        return this;
    }

    public void setParam(ShaderParam<?> param) {
        this.addParameter(param);
    }

    public void activateParams() {
        for (Map.Entry<Integer, ShaderParam<?>> entry : this.parameters.entrySet()) {
            ShaderParam<?> param = entry.getValue();
            switch (param.getType()) {
                case PARAM_FLOAT:
                    Float vFloat = (Float) param.getValue();
                    GL20.glUniform1f(entry.getKey(), vFloat);
                    break;
                case PARAM_VEC2:
                    Vector2f vVec2 = (Vector2f) param.getValue();
                    GL20.glUniform2f(entry.getKey(), vVec2.x, vVec2.y);
                    break;
                case PARAM_VEC3:
                    Vector3f vVec3 = (Vector3f) param.getValue();
                    GL20.glUniform3f(entry.getKey(), vVec3.x, vVec3.y, vVec3.z);
                    break;
                case PARAM_VEC4:
                    Vector4f vVec4 = (Vector4f) param.getValue();
                    GL20.glUniform4f(entry.getKey(), vVec4.x, vVec4.y, vVec4.z, vVec4.w);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public boolean equals(Object shader) {
        ShaderProgram shader2;
        if (shader instanceof ShaderProgram) {
            shader2 = (ShaderProgram) shader;
        } else {
            return false;
        }

        List<ResourceLocation> attachedShader2 = shader2.getAttachedShader();
        if (this.attachedShaderFiles.size() != attachedShader2.size()) {
            return false;
        } else if (this.attachedShaderFiles.size() == 0 && attachedShader2.size() == 0) {
            return false;
        } else {
            attachedShader2.removeAll(this.attachedShaderFiles);
            if (attachedShader2.size() > 0)
                return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        StringBuilder sb = new StringBuilder();
        for (ResourceLocation al : this.attachedShaderFiles) {
            sb.append(al).append(",");
        }
        return sb.toString().hashCode();
    }

    public static String loadShaderSource(ResourceLocation shaderLocation) {
        StringBuilder source = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(shaderLocation.asInputStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                source.append(line).append('\n');
            }
        } catch (IOException e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Error while loading shader \"{0}\"", shaderLocation.toString());
            e.printStackTrace();
        }

        return source.toString();
    }
}
