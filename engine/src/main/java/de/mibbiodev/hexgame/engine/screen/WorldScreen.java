package de.mibbiodev.hexgame.engine.screen;

import de.mibbiodev.hexgame.engine.render.*;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * @author mibbio
 */
public final class WorldScreen implements Screen {

    private WorldRenderer worldRenderer;
    private List<Renderable> renderableList;

    public WorldScreen(Renderer renderer) {
        this.worldRenderer = (WorldRenderer) renderer;
        this.renderableList = new ArrayList<>();
    }

    @Override
    public void addRenderable(Renderable renderable) {
        this.renderableList.add(renderable);
    }

    @Override
    public Renderer getRenderer() {
        return worldRenderer;
    }

    @Override
    public boolean isOverlay() {
        return false;
    }

    @Override
    public boolean isModal() {
        return true;
    }

    @Override
    public void update(int delta) {
        for (Renderable r : renderableList) {
            r.update(delta);
        }
        worldRenderer.update(delta);
    }

    @Override
    public void render() {
        ShaderLibrary shaderLibrary = worldRenderer.getShaderLibrary();
        int x = Mouse.getX();
        int y = Mouse.getY();

        worldRenderer.startRendering();
        for (Renderable r : renderableList) {
            r.activateShader(shaderLibrary, Renderable.RenderStage.PREPARE_STAGE);
        }

        worldRenderer.enablePick(x, y);
        for (Renderable r : renderableList) {
            r.activateShader(shaderLibrary, Renderable.RenderStage.PICKING_STAGE);
            r.drawMesh(Renderable.RenderStage.PICKING_STAGE);
        }
        Color pickingColor = worldRenderer.pickingPass(x, y);
        worldRenderer.disablePick();

        for (Renderable r : renderableList) {
            r.pick(pickingColor);
        }

        worldRenderer.render();
        for (Renderable r : renderableList) {
            r.activateShader(shaderLibrary, Renderable.RenderStage.MAIN_STAGE);
            r.drawMesh(Renderable.RenderStage.MAIN_STAGE);
        }

        worldRenderer.endRendering();
        worldRenderer.postProcesss();
    }

    @Override
    public void cleanUp() {
        // TODO Auto-generated method stub
    }
}
