package de.mibbiodev.hexgame.engine.shader;

import org.lwjgl.util.vector.*;

public final class ShaderParam<T> {

    public static enum Type {
        PARAM_FLOAT,
        PARAM_VEC2,
        PARAM_VEC3,
        PARAM_VEC4,
        PARAM_UNKNOWN
    }

    private String name;
    private T value;
    private Type type;

    public ShaderParam(String name, T value) {
        this.name = name;
        this.value = value;
        if (value instanceof Float) {
            this.type = Type.PARAM_FLOAT;
        } else if (value instanceof Vector2f) {
            this.type = Type.PARAM_VEC2;
        } else if (value instanceof Vector3f) {
            this.type = Type.PARAM_VEC3;
        } else if (value instanceof Vector4f) {
            this.type = Type.PARAM_VEC4;
        } else {
            this.type = Type.PARAM_UNKNOWN;
        }
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Type getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }
}
