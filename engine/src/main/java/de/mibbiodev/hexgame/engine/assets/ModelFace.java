package de.mibbiodev.hexgame.engine.assets;

public final class ModelFace {

    private int[] vertexIndices;
    private int[] normalIndices;

    public ModelFace() {
        this.vertexIndices = new int[3];
        this.normalIndices = new int[3];
    }

    /**
     * @param v1 Index of first vertex
     * @param v2 Index of second vertex
     * @param v3 Index of third vertex
     * @param n1 Index of first normal
     * @param n2 Index of second normal
     * @param n3 Index of third normal
     */
    public ModelFace(int v1, int v2, int v3, int n1, int n2, int n3) {
        this();
        this.vertexIndices[0] = v1;
        this.vertexIndices[1] = v2;
        this.vertexIndices[2] = v3;
        this.normalIndices[0] = n1;
        this.normalIndices[1] = n2;
        this.normalIndices[2] = n3;
    }

    public int[] getElement(int index) {
        if ((index > 0) && (index <= 3)) {
            int[] element = new int[2];
            element[0] = this.vertexIndices[index - 1];
            element[1] = this.normalIndices[index - 1];
            return element;
        } else {
            throw new IllegalArgumentException("Face has only 3 elements.");
        }
    }
}
