package de.mibbiodev.hexgame.engine.camera;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.ReadableVector3f;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.gluPerspective;

public class CameraPerspective extends AbstractCamera {
    // camera projection
    private float fov;
    private float aspect;
    private float nearClip;
    private float farClip;

    private Vector3f deltaPos;
    private Vector3f deltaRot;

    private float rotSpeed = 0.02f;
    private float moveSpeed = 0.005f;
    private float zoomSpeed = 1.0f / 3000.0f;

    private boolean leftButtonPressed = false;
    private boolean leftButtonReleased = false;
    private boolean rightButtonPressed = false;
    private boolean rightButtonReleased = false;

    public CameraPerspective(float fov, float aspect, float near, float far) {
        super();
        this.px = 0;
        this.py = 0;
        this.pz = 0;

        this.rx = 45;
        this.ry = 0;
        this.rz = 0;

        this.rotPivot = new Vector3f(0f, 0f, 0f);

        this.fov = fov;
        this.aspect = aspect;
        this.nearClip = near;
        this.farClip = far;

        deltaPos = new Vector3f(0, 0, 0);
        deltaRot = new Vector3f(0, 0, 0);
    }

    public void initProjection() {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluPerspective(fov, aspect, nearClip, farClip);
        glMatrixMode(GL_MODELVIEW);
    }

    @Override
    public void update(int delta) {
        ry = (ry + deltaRot.y * rotSpeed * delta) % 360.0f;
        ry = (ry < 0) ? 360.0f - ry : ry;
        moveAlongX(deltaPos.x * moveSpeed * delta);
        moveAlongZ(deltaPos.z * moveSpeed * delta);
        moveAlongY(deltaPos.y * zoomSpeed * delta);
        deltaPos.set(0, 0, 0);
        deltaRot.set(0, 0, 0);
    }

    @Override
    public void setView() {
        super.setView();
        glTranslatef(0, -10, 0);
    }

    @Override
    public void moveAlongX(float distance) {
        px -= distance * (float) Math.sin(Math.toRadians(ry - 90));
        pz += distance * (float) Math.cos(Math.toRadians(ry - 90));
    }

    @Override
    public void moveAlongZ(float distance) {
        px += distance * (float) Math.sin(Math.toRadians(ry));
        pz -= distance * (float) Math.cos(Math.toRadians(ry));
    }

    @Override
    public void moveAlongY(float distance) {
        if (py + distance <= 0.0f) {
            py += distance;
        }
    }

    @Override
    public Integer[] getRequestedPolls() {
        List<Integer> keys = new ArrayList<>();
        keys.add(Keyboard.KEY_LEFT);
        keys.add(Keyboard.KEY_RIGHT);
        keys.add(Keyboard.KEY_UP);
        keys.add(Keyboard.KEY_DOWN);
        keys.add(Keyboard.KEY_R);
        return keys.toArray(new Integer[keys.size()]);
    }

    @Override
    public Integer[] getRequestedEvents() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean usesMouse() {
        return true;
    }

    @Override
    public void mouseButtonStatus(boolean buttonLeft, boolean buttonRight) {
        if (!buttonRight && !rightButtonReleased) rightButtonReleased = true;

        if (buttonLeft) {
            deltaPos.x += Mouse.getDX();
            deltaPos.z += Mouse.getDY();
        } else if (buttonRight) {
            /*
             * TODO Kamerakontrollen
             * -Mausposition in Winkel umrechnen vom Kreis mit Mittelpunkt Bildmitte
             *
             * -Mausrad: Blickwinkel
             * -LMB: Verschieben
             * -RMB: Drehen
             * -LMB+RMB / Radklick: "Zoomen"
             */

            if (rightButtonReleased) {
                rightButtonReleased = false;
                // TODO find rotation center
            }



            if (Mouse.getY() >= Display.getHeight() / 2) {
                deltaRot.y -= Mouse.getDX();
            } else {
                deltaRot.y += Mouse.getDX();
            }

            if (Mouse.getX() >= Display.getWidth() / 2) {
                deltaRot.y += Mouse.getDY();
            } else {
                deltaRot.y -= Mouse.getDY();
            }
        }
        deltaPos.y = Mouse.getDWheel();
    }
}
