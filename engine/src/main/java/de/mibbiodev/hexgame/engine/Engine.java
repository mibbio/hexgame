package de.mibbiodev.hexgame.engine;

import de.mibbiodev.hexgame.engine.control.ControlHandler;
import de.mibbiodev.hexgame.engine.event.Listener;
import de.mibbiodev.hexgame.engine.logging.EngineLogger;
import de.mibbiodev.hexgame.engine.state.State;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.*;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class Engine implements Listener {

    public static int SCREEN_WIDTH = 0;
    public static int SCREEN_HEIGHT = 0;

    private final int[] glVersion;
    private String title;

    // fps values
    private int[] delta = new int[2];
    private long[] fps = new long[2];
    private long timeLastFrame;

    private boolean running = false;

    private static Proxy proxy;

    public Engine(String title, int width, int height, int[] glVersion, Class<?> mainClass) {
        this.title = title;
        this.glVersion = glVersion;
        this.delta[0] = 1;
        this.delta[1] = 1;
        this.fps[0] = 60;
        this.fps[1] = getTime();
        getDelta();

        proxy = new Proxy(new ControlHandler(), mainClass);

        SCREEN_WIDTH = width;
        SCREEN_HEIGHT = height;

        initDisplay(width, height);
    }

    private void initDisplay(int width, int height) {
        try {
            int currentBpp = Display.getDisplayMode().getBitsPerPixel();
            DisplayMode dm = findDisplayMode(width, height, currentBpp);
            System.out.println(dm);
            Display.setDisplayMode(dm);
            Display.setTitle(title);
            PixelFormat pf = new PixelFormat().withSamples(4).withDepthBits(16);
            ContextAttribs attribs = new ContextAttribs(glVersion[0], glVersion[1]).withProfileCompatibility(true);
            // TODO fuer 3.2 auf MacOSX: ContextAttrib(3, 2).withProfileCore()
            Display.create(pf, attribs);
            GL11.glViewport(0, 0, width, height);
            System.out.println("OpenGL Version: " + GL11.glGetString(GL11.GL_VERSION));
        } catch (LWJGLException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }

    private int getDelta() {
        delta[1] = delta[0];
        long time = getTime();
        delta[0] = (int) (time - timeLastFrame);
        timeLastFrame = time;
        return delta[0];
    }

    public void updateFPS() {
        if (getTime() - fps[1] > 1000) {
            Display.setTitle(title + " - FPS: " + fps[0]);
            fps[0] = 0;
            fps[1] += 1000;
        }
        fps[0]++;
    }

    public void start(State initialState) {
        this.running = this.proxy.getStateManager().setState(initialState);
    }

    public void update() {
        if (proxy.getStateManager().getCurrentState() == null || !proxy.getStateManager().hasStates()) running = false;
        if (!running) return;

        int delta = getDelta();
        proxy.getControlHandler().updateInput(delta);
        proxy.getStateManager().getCurrentState().update(delta);
        this.running = !Display.isCloseRequested();
    }

    public void render() {
        if (!running) return;
        proxy.getStateManager().getCurrentState().render();
        Display.update();
        Display.sync(60);
        updateFPS();
    }

    public boolean isRunning() {
        return running;
    }

    private DisplayMode findDisplayMode(int width, int height, int bpp) {
        try {
            for (DisplayMode dm : Display.getAvailableDisplayModes()) {
                if ((dm.getBitsPerPixel() == bpp) && (dm.getHeight() == height) && (dm.getWidth() == width)) {
                    return dm;
                }
            }
        } catch (LWJGLException e) {
            return new DisplayMode(width, height);
        }
        return new DisplayMode(width, height);
    }

    public int close() {
        proxy.getStateManager().cleanUp();
        Display.destroy();
        return 0;
    }

    public static EngineLogger getLogger(String logFile) {
        return EngineLogger.getLogger(logFile);
    }

    public static final Proxy getCoreProxy() {
        if (proxy == null) {
            throw new IllegalStateException("There is no proxy available. Create an instance of Engine first.");
        }
        return proxy;
    }
}
