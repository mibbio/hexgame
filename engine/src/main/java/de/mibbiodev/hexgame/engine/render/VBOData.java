package de.mibbiodev.hexgame.engine.render;

/**
 * @author mibbio
 */
public class VBOData {

    public final int bufferId;
    public int elementCount;
    public int vertexSize;

    public VBOData(int bufferId) {
        this.bufferId = bufferId;
    }

    public final int getBufferId() {
        return bufferId;
    }

    public final int getElementCount() {
        return elementCount;
    }

    public final void setElementCount(int elementCount) {
        this.elementCount = elementCount;
    }

    public final int getVertexSize() {
        return vertexSize;
    }

    public final void setVertexSize(int vertexSize) {
        this.vertexSize = vertexSize;
    }
}
