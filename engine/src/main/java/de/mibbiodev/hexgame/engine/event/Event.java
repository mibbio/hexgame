package de.mibbiodev.hexgame.engine.event;

/**
 * @author mibbio
 */
public abstract class Event {

    private final Object emitter;

    protected Event(Object emitter) {
        this.emitter = emitter;
    }

    public final Object getEmitter() {
        return this.emitter;
    }
}
