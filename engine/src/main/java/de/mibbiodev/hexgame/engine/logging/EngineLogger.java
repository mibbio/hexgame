package de.mibbiodev.hexgame.engine.logging;

import de.mibbiodev.hexgame.engine.Engine;

import java.io.IOException;
import java.util.logging.*;

/**
 * @author mibbio
 */
public class EngineLogger {

    private static Logger logger;
    private static final EngineLogger INSTANCE = new EngineLogger();

    private static boolean initialized = false;
    private static String logFile;

    private EngineLogger() {}

    public static final EngineLogger getLogger(String logFile) {
        if (!initialized) {
            initLogger();
        }
        return INSTANCE;
    }

    private static void initLogger() {
        logger = Logger.getLogger(Engine.class.getName());
        logger.setUseParentHandlers(false);

        if (logFile != "" && logFile != null) {
            try {
                Handler fileHandler = new FileHandler(logFile, false);
                logger.addHandler(fileHandler);
            } catch (IOException e) {
                logFile = null;
                e.printStackTrace();
            }
        }

        initialized = true;
    }
}
