package de.mibbiodev.hexgame.engine.assets;

import de.mibbiodev.hexgame.engine.render.VBOData;
import de.mibbiodev.hexgame.engine.utils.Transform;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.util.vector.Vector3f;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;


public class GameModel implements GameAsset {

    private static final byte POSITION_SIZE = 3;
    private static final byte NORMAL_SIZE = 3;
    private static final byte COLOR_SIZE = 3;
    public static final byte VERTEX_SIZE = POSITION_SIZE + NORMAL_SIZE + COLOR_SIZE;

    private final int id;
    private final String name;

    private final List<Integer> shader = new ArrayList<>();

    private int vboVertices = 0;
    private int elementCount = 0;

    public GameModel(int modelId, String modelName, List<Vector3f> vertexValues, List<Vector3f> normalValues, List<ModelFace> faceIndices) {
        this.id = modelId;
        this.name = modelName;
        this.createVBO(vertexValues, normalValues, faceIndices);
    }

    public void attachShader(int shaderID) {
        this.shader.add(shaderID);
    }

    public Integer[] getAttachedShaders() {
        return this.shader.toArray(new Integer[this.shader.size()]);
    }


    private void createVBO(List<Vector3f> vertexValues, List<Vector3f> normalValues, List<ModelFace> faceIndices) {
        FloatBuffer fb = BufferUtils.createFloatBuffer(VERTEX_SIZE * faceIndices.size() * 3);
        for (ModelFace face : faceIndices) {
            for (int i = 1; i <= 3; i++) {
                int[] indices = face.getElement(i);
                Vector3f vertex = vertexValues.get(indices[0]);
                Vector3f normal = normalValues.get(indices[1]);
                Vector3f color = new Vector3f(0.8f - normal.x, 0.8f - normal.y, 0.8f - normal.z);

                vertex.store(fb);
                normal.store(fb);
                color.store(fb);
            }
        }
        fb.flip();
        this.elementCount = faceIndices.size() * 3;

        vboVertices = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboVertices);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, fb, GL15.GL_STATIC_DRAW);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    }

    public VBOData getVBOData() {
        VBOData data = new VBOData(vboVertices);
        data.setElementCount(elementCount);
        data.setVertexSize(VERTEX_SIZE);
        return data;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int compareTo(GameAsset asset) {
        return this.name.compareToIgnoreCase(asset.getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof GameModel) {
            GameModel gm = (GameModel) obj;
            return gm.getName().equals(this.name);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}
