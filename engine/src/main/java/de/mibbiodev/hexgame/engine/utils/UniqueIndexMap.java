package de.mibbiodev.hexgame.engine.utils;

import java.util.Map.Entry;
import java.util.TreeMap;

public final class UniqueIndexMap<K extends Integer, V> extends TreeMap<Integer, V> {

    private static final long serialVersionUID = 1L;
    private int lastIndex;

    public UniqueIndexMap(int lowestIndex) {
        super();
        this.lastIndex = lowestIndex - 1;
    }

    public int put(V value) {
        if (this.lastIndex < Integer.MAX_VALUE) {
            this.lastIndex++;
            super.put(this.lastIndex, value);
            return this.lastIndex;
        }
        return 0;
    }

    public V alter(Integer key, V value) {
        return super.put(key, value);
    }

    public int getKeyByValue(V value) {
        for (Entry<Integer, V> entry : this.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return -1;
    }

    @Override
    public V put(Integer key, V value) {
        throw new UnsupportedOperationException("use alter(K key, V value)");
    }
}
