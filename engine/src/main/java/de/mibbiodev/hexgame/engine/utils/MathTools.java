package de.mibbiodev.hexgame.engine.utils;

import org.lwjgl.util.vector.Vector3f;

/**
 * @author mibbio
 */
public class MathTools {

    public static final float coTangent(float angle) {
        return (float) (1.0f / Math.tan(angle));
    }

    public static final float degreesToRadians(float angle) {
        return (float) (angle * (Math.PI / 180.0f));
    }

    public static final Vector3f xAxisVector() {
        return new Vector3f(1, 0, 0);
    }

    public static final Vector3f yAxisVector() {
        return new Vector3f(0, 1, 0);
    }

    public static final Vector3f zAxisVector() {
        return new Vector3f(0, 0, 1);
    }
}
