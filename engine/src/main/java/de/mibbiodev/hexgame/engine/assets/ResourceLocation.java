package de.mibbiodev.hexgame.engine.assets;

import de.mibbiodev.hexgame.engine.Engine;

import java.io.*;

/**
 * @author mibbio
 */
public final class ResourceLocation {

    private final String file;
    private final Class<?> root;

    public ResourceLocation(String file, Class<?> root) {
        this.file = file;
        this.root = root;
    }

    public final InputStream asInputStream() throws FileNotFoundException {
        InputStream inputStream = null;
        if (root == null) {
            inputStream = new FileInputStream(file);
        } else {
            inputStream = root.getResourceAsStream(file);
            if (inputStream == null) throw new FileNotFoundException("Resource '" + file + "' not found.");
        }
        return inputStream;
    }

    @Override
    public String toString() {
        return file;
    }
}
