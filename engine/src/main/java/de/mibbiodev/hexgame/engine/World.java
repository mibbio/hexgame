package de.mibbiodev.hexgame.engine;

import de.mibbiodev.hexgame.engine.assets.*;
import de.mibbiodev.hexgame.engine.entity.BasicEntity;
import de.mibbiodev.hexgame.engine.entity.TestEntity;
import de.mibbiodev.hexgame.engine.render.EntityRenderer;
import de.mibbiodev.hexgame.engine.screen.Screen;
import de.mibbiodev.hexgame.engine.shader.ShaderProgram;
import de.mibbiodev.hexgame.engine.terrain.Terrain;
import de.mibbiodev.hexgame.engine.terrain.TerrainTile;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.Color;

import java.util.*;

public class World {

    private class SandTile extends TerrainTile {
        public SandTile(float height) {
            super(height);
        }

        @Override
        public Color getColor() {
            return Color.orange;
        }
    }

    private class GrassTile extends TerrainTile {
        private GrassTile(float height) {
            super(height);
        }

        @Override
        public Color getColor() {
            return Color.green;
        }
    }

    private class WaterTile extends TerrainTile {
        private WaterTile(float height) {
            super(height);
        }

        @Override
        public Color getColor() {
            return Color.blue;
        }
    }

    private Map<String, Integer> entities = new HashMap<>();
    private Terrain terrain;

    public World(ResourceLocation worldFile, Screen screen) throws Exception {
        loadEntities();
        terrain = loadWorld(worldFile);
        terrain.setupShader(screen.getRenderer().getShaderLibrary());
        screen.addRenderable(terrain);

        // loading and setting default shader for game models
        ShaderProgram sp = new ShaderProgram();
        sp.attachShader(new ResourceLocation("/glsl/simple.vert",
                Engine.getCoreProxy().getRootClass()),
                GL20.GL_VERTEX_SHADER
        );
        sp.attachShader(new ResourceLocation("/glsl/simple.frag",
                Engine.getCoreProxy().getRootClass()),
                GL20.GL_FRAGMENT_SHADER
        );
        BasicEntity.DEFAULT_ENTITY_SHADER = screen.getRenderer().getShaderLibrary().addShaderProgram(sp);

        // test data
        AssetManager assets = Engine.getCoreProxy().getAssetManager();
        TestEntity t1 = new TestEntity(assets.getAsset(
                AssetType.MODEL,
                "t1",
                new ResourceLocation("/models/flatten.obj", Engine.getCoreProxy().getRootClass())
        ));

        TestEntity t2 = new TestEntity(assets.getAssetByName("t1").getId());
        t2.getTransform().setPosition(new Vector3f(10f, 5f, -20f));
        t2.getTransform().scale(0.25f);

        TestEntity t3 = new TestEntity(assets.getAssetByName("t1").getId());
        t3.getTransform().setPosition(new Vector3f(30f, 10f, -20f));
        t3.getTransform().scale(0.5f);

        TestEntity t4 = new TestEntity(assets.getAssetByName("t1").getId());
        t4.getTransform().setPosition(new Vector3f(40f, 20f, -40f));

        screen.addRenderable(new EntityRenderer(t1));
        screen.addRenderable(new EntityRenderer(t2));
        screen.addRenderable(new EntityRenderer(t3));
        screen.addRenderable(new EntityRenderer(t4));
        // end test data
    }

    public final void update(int delta) {
        terrain.update(delta);
    }

    private Terrain loadWorld(ResourceLocation worldFile) throws Exception {
        // TODO width / height aus worldFile auslesen
        // TODO remove testdata
        int width = 64;
        int height = 64;

        Random random = new Random(42);

        TerrainTile[][] tiles = new TerrainTile[width][height];
        for (int x = 0; x < tiles.length; x++) {
            for (int y = 0; y < tiles[0].length; y++) {
                float tile = random.nextFloat();
                if (tile < 0.3f) tiles[x][y] = new SandTile(2.0f + new Random().nextFloat());
                else if (tile < 0.8f) tiles[x][y] = new GrassTile(2.0f + new Random().nextFloat());
                else tiles[x][y] = new WaterTile(2.0f + new Random().nextFloat());
            }
        }

        return new Terrain(width, height, tiles);
}

    private void loadEntities() {
        //int id = EntityManager.createExtendableEntity(AssetManager.getInstance(), EntityTank.class, 0, 0);
    }

    public void cleanUp() {

    }
}
