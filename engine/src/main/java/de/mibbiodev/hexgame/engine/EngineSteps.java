package de.mibbiodev.hexgame.engine;

/**
 * @author mibbio
 */
public interface EngineSteps {

    void update(int delta);

    void render();

    void cleanUp();
}
