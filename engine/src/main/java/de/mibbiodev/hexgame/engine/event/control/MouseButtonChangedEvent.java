package de.mibbiodev.hexgame.engine.event.control;

import de.mibbiodev.hexgame.engine.event.Event;

/**
 * @author mibbio
 */
public class MouseButtonChangedEvent extends Event {

    private final MouseButtonStatus buttonStatus;
    private final int buttonNumber;

    public MouseButtonChangedEvent(int buttonNumber, MouseButtonStatus buttonStatus, Object emitter) {
        super(emitter);
        this.buttonNumber = buttonNumber;
        this.buttonStatus = buttonStatus;
    }

    int getButtonNumber() {
        return buttonNumber;
    }

    MouseButtonStatus getButtonStatus() {
        return buttonStatus;
    }
}
