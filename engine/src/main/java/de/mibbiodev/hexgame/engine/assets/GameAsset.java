package de.mibbiodev.hexgame.engine.assets;

public interface GameAsset extends Comparable<GameAsset> {

    int getId();

    String getName();
}
