package de.mibbiodev.hexgame.engine.camera;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

public abstract class AbstractCamera implements CameraInterface {

    protected final int id;

    // position
    protected float px;
    protected float py;
    protected float pz;

    // rotation
    protected float rx;
    protected float ry;
    protected float rz;

    protected Vector3f rotPivot;

    public AbstractCamera() {
        this.id = 1;
    }

    @Override
    public final int getId() {
        return this.id;
    }

    @Override
    public void setView() {
        GL11.glLoadIdentity();
        GL11.glRotatef(rx, 1, 0, 0);
        GL11.glRotatef(ry, 0, 1, 0);
        GL11.glRotatef(rz, 0, 0, 1);
        GL11.glTranslatef(px, py, pz);
    }

    @Override
    public void setRotation(Vector3f rotation) {
        this.rx = rotation.x;
        this.ry = rotation.y;
        this.rz = rotation.z;
    }

    @Override
    public void setPosition(Vector3f position) {
        this.px = position.x;
        this.py = position.y;
        this.pz = position.z;
    }

}
