package de.mibbiodev.hexgame.engine.entity;

import java.util.List;
import java.util.Random;

/**
 * @author mibbio
 */
public class TestEntity extends BasicEntity {

    private static Random RANDOM = new Random();
    private float rotSpeed;

    public TestEntity(int modelId) {
        super(modelId);
        rotSpeed = RANDOM.nextFloat();
    }

    @Override
    public List<Entity> getAttachedEntities() {
        return null;  // TODO Auto-generated method stub
    }

    @Override
    public void update(int delta) {
        transform.rotate(0, delta * rotSpeed / 5f, 0);
    }

    @Override
    public void cleanUp() {
        // TODO Auto-generated method stub
    }
}
