package de.mibbiodev.hexgame.engine.event;

import java.util.ArrayList;
import java.util.EnumMap;

/**
 * @author mibbio
 */
public class EventBus {

    private final EnumMap<EventPriority, ArrayList<Listener>> listeners;

    public EventBus() {
        listeners = new EnumMap<>(EventPriority.class);
        for (EventPriority eventPriority : EventPriority.values()) {
            listeners.put(eventPriority, new ArrayList<Listener>());
        }
    }
    /*
    public void addListener(Listener listener) {
        if (listeners.get(listener.getPriority()).contains(listener)) {
            throw new IllegalStateException("This listener is already registered to priority " + listener.getPriority().toString());
        }
        listeners.get(listener.getPriority()).add(listener);
    } */
}
