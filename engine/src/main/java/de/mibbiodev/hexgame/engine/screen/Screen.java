package de.mibbiodev.hexgame.engine.screen;

import de.mibbiodev.hexgame.engine.EngineSteps;
import de.mibbiodev.hexgame.engine.render.Renderable;
import de.mibbiodev.hexgame.engine.render.Renderer;

/**
 * @author mibbio
 */
public interface Screen extends EngineSteps {

    void addRenderable(Renderable renderable);

    Renderer getRenderer();

    boolean isOverlay();

    boolean isModal();
}
