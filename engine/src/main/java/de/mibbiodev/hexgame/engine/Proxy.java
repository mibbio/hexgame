package de.mibbiodev.hexgame.engine;

import de.mibbiodev.hexgame.engine.assets.AssetManager;
import de.mibbiodev.hexgame.engine.control.ControlHandler;
import de.mibbiodev.hexgame.engine.event.EventBus;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;
import de.mibbiodev.hexgame.engine.state.StateManager;

/**
 * @author mibbio
 */
public class Proxy {

    private final ControlHandler controlHandler;
    private final StateManager stateManager;
    private final AssetManager assetManager;
    private final EventBus eventBus;
    private final Class<?> rootClass;

    public Proxy(ControlHandler controlHandler, Class<?> rootClass) {
        this.stateManager = new StateManager(new ShaderLibrary(), controlHandler);
        this.controlHandler = controlHandler;
        this.eventBus = new EventBus();
        this.assetManager = new AssetManager();
        this.rootClass = rootClass;
    }

    public final ControlHandler getControlHandler() {
        return controlHandler;
    }

    public final StateManager getStateManager() {
        return stateManager;
    }

    public final AssetManager getAssetManager() {
        return assetManager;
    }

    public final Class<?> getRootClass() {
        return rootClass;
    }

    public EventBus getEventBus() {
        return eventBus;
    }

    public void update(int delta) {
        controlHandler.updateInput(delta);
        stateManager.getCurrentState().update(delta);
    }

    public void cleanUp() {
        stateManager.cleanUp();
    }
}
