package de.mibbiodev.hexgame.engine.render;

import de.mibbiodev.hexgame.engine.entity.BasicEntity;
import de.mibbiodev.hexgame.engine.entity.Entity;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;
import org.newdawn.slick.Color;

/**
 * @author mibbio
 */
public class EntityRenderer implements Renderable {

    private final Entity attachedEntity;

    public EntityRenderer(Entity entity) {
        this.attachedEntity = entity;
    }

    @Override
    public void update(int delta) {
        attachedEntity.update(delta);
        // TODO Auto-generated method stub
    }

    @Override
    public void pick(Color color) {
        // TODO Auto-generated method stub
    }

    @Override
    public void drawMesh(RenderStage renderStage) {
        if (renderStage == RenderStage.MAIN_STAGE) {
            attachedEntity.render();
        }
    }

    @Override
    public void activateShader(ShaderLibrary shaderLibrary, RenderStage renderStage) {
        if (renderStage == RenderStage.MAIN_STAGE) {
            shaderLibrary.activateShaderProgram(BasicEntity.DEFAULT_ENTITY_SHADER);
        }
    }

    @Override
    public void setupShader(ShaderLibrary shaderLibrary) {
        // TODO Auto-generated method stub
    }

    @Override
    public void cleanUp(ShaderLibrary shaderLibrary) {
        // TODO Auto-generated method stub
    }
}
