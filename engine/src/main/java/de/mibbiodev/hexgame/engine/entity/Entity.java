package de.mibbiodev.hexgame.engine.entity;

import de.mibbiodev.hexgame.engine.EngineSteps;
import de.mibbiodev.hexgame.engine.assets.GameModel;
import de.mibbiodev.hexgame.engine.utils.Transform;

import java.util.List;

/**
 * @author mibbio
 */
public interface Entity extends EngineSteps {

    Transform getTransform();

    GameModel getModel();

    List<Entity> getAttachedEntities();

}
