uniform vec3 LightPosition;

varying vec3 LightVec;
varying vec3 ecPos;

varying vec3 myNormal;
varying vec3 myVertex;

void main() {
	vec3 ecPos    = vec3(gl_ModelViewProjectionMatrix * gl_Vertex);
	LightVec = normalize(LightPosition - ecPos);

    myNormal = gl_Normal;
    myVertex = gl_Vertex.xyz;
    gl_FrontColor = gl_SecondaryColor;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}