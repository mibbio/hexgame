uniform float Selection;
uniform vec3 SurfaceColor;
uniform vec3 WarmColor;
uniform vec3 CoolColor;
uniform float DiffuseWarm;
uniform float DiffuseCool;

varying vec3 ecPos;
varying vec3 LightVec;

varying vec3 myNormal;
varying vec3 myVertex;

void main() {
	vec3 norm = myNormal;

	if (mod(gl_PrimitiveID, 16.0) >= 4.0) {
		norm = normalize(cross(dFdx(myVertex), dFdy(myVertex)));
	}

	vec3 tnorm 		   = normalize(gl_NormalMatrix * norm);
	vec3 ReflectVec    = normalize(reflect(-LightVec, tnorm));
    vec3 ViewVec       = normalize(-ecPos);
    float NdotL        = (dot(LightVec, tnorm)) * 0.75;
    
    vec3 kcool	= min(CoolColor + DiffuseCool * SurfaceColor, 0.9);
	vec3 kwarm	= min(WarmColor + DiffuseWarm * SurfaceColor, 0.7);
	vec3 kfinal	= mix(kcool, kwarm, NdotL);

	vec3 nreflect = normalize(ReflectVec);
	vec3 nview	  = normalize(ViewVec);

	float spec	= max(dot(nreflect, nview), 0.0);
	spec		= pow(spec, 32.0);
	
	float selVert = 16*Selection;
	vec3 color = vec3(min(kfinal, 1.0));

	if (gl_PrimitiveID >= selVert && gl_PrimitiveID < selVert+16) {
		gl_FragColor = vec4(vec3(1.0, 0.6, 0.0) - color, 1.0);
	} else {
		gl_FragColor = vec4(kfinal, 1.0) + vec4(gl_Color.rgb / 1.75, 1.0);
	}
	
}