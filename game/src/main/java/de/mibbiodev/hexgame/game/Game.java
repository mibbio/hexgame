package de.mibbiodev.hexgame.game;

import de.mibbiodev.hexgame.engine.Engine;
import de.mibbiodev.hexgame.engine.assets.*;
import de.mibbiodev.hexgame.engine.control.ControlHandler;
import de.mibbiodev.hexgame.engine.shader.ShaderLibrary;
import de.mibbiodev.hexgame.engine.state.GameState;
import de.mibbiodev.hexgame.engine.state.State;

import java.io.File;
import java.io.IOException;

public class Game {

static {
    String nativePath = "";
    try {
        nativePath = new File(".").getCanonicalPath()
                + File.separator + "natives";
    } catch (IOException e) {
        e.printStackTrace();
    }
    System.setProperty("org.lwjgl.librarypath", nativePath);
}
	
    private static int WIDTH = 1280;
    private static int HEIGHT = 720;
    private static String GAME_TITLE = "HexGame (internal preAlpha)";
    private static int[] GL_VERSION = new int[]{3, 2};
    
    private Engine engine;
    private State GamePlay = new GameState(null);

    public Game() {
        engine = new Engine(GAME_TITLE, WIDTH, HEIGHT, GL_VERSION, Game.class);
        // Test code for loading YAML files
        /*
        InputStream yamlInput = Game.class.getResourceAsStream("/yaml/modules.yml");
        if (yamlInput != null) {
            Yaml yaml = new Yaml();
            Map<String, LinkedHashMap> content = (LinkedHashMap<String, LinkedHashMap>) yaml.load(yamlInput);

            Map<String, LinkedHashMap> modules = (LinkedHashMap<String, LinkedHashMap>) content.get("modules");
            System.out.println(modules);
        }
        */

    }

    private void run() {
        engine.start(GamePlay);
        while (engine.isRunning()) {
            engine.update();
            engine.render();
        }
        engine.close();
    }

    public static void main( String[] args ) {
        Game game = new Game();
        game.run();
    }
}
