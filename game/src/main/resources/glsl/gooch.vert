// simple vertex shader

// Values
// ------
// CoolColor:		0.20 0.20 0.00
// DiffuseCool:		0.30
// DiffuseWarm:		0.60
// SurfaceColor:	1.00 0.75 0.75
// WarmColor:		0.60 0.60 0.00


uniform vec3 LightPosition;

varying float NdotL;
varying vec3 ReflectVec;
varying vec3 ViewVec;

void main()
{
    //vec3 ecPos    = vec3(gl_ModelViewProjectionMatrix * gl_Vertex);
    vec3 tnorm    = normalize(gl_NormalMatrix * gl_Normal);
    //vec3 lightVec = normalize(LightPosition - ecPos);
    //ReflectVec    = normalize(reflect(-lightVec, tnorm));
    //ViewVec       = normalize(-ecPos);
    NdotL         = (dot(lightVec, tnorm)) * 0.75;

    gl_Position   = ftransform();
}
